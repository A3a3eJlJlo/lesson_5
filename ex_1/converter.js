/**
 * Created by Owner on 31.01.2017.
 */

function convertDecToHex(r, g, b)
{
    var obj = {
        red: r.toString(16),
        green: g.toString(16),
        blue: b.toString(16)
    };

    return obj;
}

console.log(convertDecToHex(10, 11, 12));