/**
 * Created by Owner on 31.01.2017.
 */
function ordersCounter(num)
{
    if(num < 0 || num > 999) {
        console.log("Wrong number.");
        return;
    }

    var obj = {
        hundreds: 0,
        tens: 0,
        ones: 0
    };

    obj.ones = num % 10;
    num = num / 10 | 0;
    obj.tens = num % 10;
    num = num / 10 | 0;
    obj.hundreds = num % 10;

    return obj;
}

console.log(ordersCounter(123));