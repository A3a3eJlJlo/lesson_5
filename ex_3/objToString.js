/**
 * Created by Owner on 31.01.2017.
 */

function objToString(obj)
{
    var resultStr = "";
    var counter = 0;
    for(var key in obj)
    {
        if(counter)
            resultStr += "&";
        resultStr += key;
        resultStr += "=";
        resultStr += obj[key];
        ++counter;
    }

    return resultStr;
}

var myObj = {
    name: "Evgeniy",
    age: "100",
    id: 1
};

console.log(objToString(myObj));